<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('users', 'UserController')->middleware('auth:api');
Route::get('articles/tags', 'ArticleController@indexByTags')->middleware('auth:api');
Route::resource('articles', 'ArticleController')->middleware('auth:api');
Route::get('articles/categories/{id}', 'ArticleController@indexByCategorie')->middleware('auth:api');
Route::get('articles/users/{id}', 'ArticleController@indexByUser')->middleware('auth:api');
Route::resource('tags', 'TagController')->middleware('auth:api');
Route::resource('categories', 'CategorieController')->middleware('auth:api');

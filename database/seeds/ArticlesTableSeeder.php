<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 20;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('articles')->insert([ //,
                'name' => $faker->unique()->name,
                'description' => $faker->text($maxNbChars = 191),
                'img' => $faker->imageUrl($width = 640, $height = 480),
                'categorie_id' => $faker->numberBetween(1,5),
                'user_id' => $faker->numberBetween(1,10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }


    }
}

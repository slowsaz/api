<?php

namespace App\Http\Controllers;

use App\Article;
use App\Categorie;
use App\Http\Resources\Categorie as OneCategorie;
use App\Http\Requests\StoreArticle;
use App\Http\Resources\ArticleCollection;
use App\Http\Resources\Article as OneArticle;
use App\Http\Resources\CategorieCollection;
use App\Http\Resources\TagCollection;
use App\Http\Resources\UserCollection;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ArticleCollection(Article::all());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArticle $request)
    {
        $article = Article::create($request->all());
        return new OneArticle($article);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::with('Tags','Categorie','User')->find($id);
        return new OneArticle($article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::find($id);
        $article->update($request->all());
        return new OneArticle($article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        if(!$article){
            return ('Article not found');
        }
        $article->delete();

        return response()->json(null, 204);
    }

    public function indexByCategorie($id)
    {
        $categorie = Categorie::find($id)->articles;
        return new CategorieCollection($categorie);
    }

    public function indexByUser($id)
    {
        $user = User::find($id)->articles;
        return new UserCollection($user);
    }

    public function indexByTags()
    {
        $tagId = ( explode( ',', Input::get('tags')));
        $articles = Article::whereHas('tags',function($query) use ($tagId){
            $query->whereIn('tag_id',$tagId);
        })->get();
        return new TagCollection($articles);
    }
}

<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation;


class StoreArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:articles|max:191',
            'description' => 'required|max:191',
            'img' => 'required|max:191',
            'categorie_id' => 'required|numeric',
            'user_id' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => 'L\'article ne doit pas être présent dans la base de données',
            'name.required'=>'Le nom de l\'aarticle doit être saisi',
            'description.required'=>'La description de l\'article doit être saisie',
            'img.required' => 'L\'article doit avoir une image',
            'categorie_id.required'  => 'L\'article doit avoir une catégorie',
            'user_id.required'  => 'L\'article doit avoir un utilisateur',
        ];
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw (new HttpResponseException(response()->json($validator->errors(),422)));
    }
}

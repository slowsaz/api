<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreCategorie extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:categories|max:191',
            'description' => 'required|max:191'
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => 'La catégorie ne doit pas être présente dans la base de données',
            'name.required'=>'Le nom de la catégorie doit être saisi',
            'description.required'=>'La description de la catégorie doit être saisie',
        ];
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw (new HttpResponseException(response()->json($validator->errors(),422)));
    }
}

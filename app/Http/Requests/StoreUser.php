<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:users|max:191',
            'email' => 'required|email',
            'password' => 'required|max:191'
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => 'L\'article ne doit pas être présent dans la base de données',
            'name.required'=>'Le nom de l\'aarticle doit être saisi',
            'email.required'=>'L\'adresse doit être saisie',
            'email.email' => 'L\'adresse mail n\'est pas valide',
            'password.required' => 'Un mot de passe est requis',
        ];
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw (new HttpResponseException(response()->json($validator->errors(),422)));
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['name','description','img','categorie_id','user_id','created_at','updated_at'];

    public function tags(){
        return $this->belongsToMany('App\Tag');
    }

    public function categorie(){
        return $this->belongsTo('App\Categorie');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function getUrlAttribute(){
        return "https://127.0.0.1:8000/api/articles/".$this->id;
    }

}
